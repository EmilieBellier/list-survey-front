import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurveyModalComponent } from './survey-modal/survey-modal.component';
import { SurveysService } from './surveys.service';
import { SurveyComponent } from './survey/survey.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SurveyComponent, SurveyModalComponent],
  exports: [SurveyComponent,],
  providers: [SurveysService],
  entryComponents : [SurveyModalComponent]
})
export class SurveysModule { }
