import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as Survey from "survey-angular";
import * as widgets from "surveyjs-widgets";
import { ListesService } from '../../listes/listes.service';
import { Observable } from 'rxjs/Observable';
import { List } from '../../listes/data-model';
Survey.Survey.cssType = "bootstrap";

widgets.icheck(Survey);
widgets.select2(Survey);
widgets.imagepicker(Survey);
widgets.inputmask(Survey);
widgets.jquerybarrating(Survey);
widgets.jqueryuidatepicker(Survey);
widgets.nouislider(Survey);
widgets.select2tagbox(Survey);
widgets.signaturepad(Survey);
widgets.sortablejs(Survey);
widgets.ckeditor(Survey);
widgets.autocomplete(Survey);
widgets.bootstrapslider(Survey);

@Component({
  selector: 'survey-modal',
  templateUrl: './survey-modal.component.html',
  styleUrls: ['./survey-modal.component.css']
})
export class SurveyModalComponent implements OnInit {

  listGet: {} = JSON.parse(JSON.stringify(this.listsService.getLists2()));
  @Input() json: Object; //récupérable avec this.json
  @Input() contenuCopy: any[];

  resForm: Object;
  surveyModel: Survey.Survey;

  constructor(public activeModal: NgbActiveModal,
    private listsService: ListesService) { }

  ngOnInit() {
    this.createForm();
    if (this.contenuCopy) {
      for (var key in this.contenuCopy) {
        console.log('param contenu ', this.contenuCopy[key], 'param key ', key);
        this.surveyModel.setValue(key, this.contenuCopy[key]);
      }
    }
  }

  createForm(){
    for (let i = 0; i < Object.keys(this.listGet).length; i++) {
      this.json['pages'][0]['elements'][i + 1].name = this.listGet[i].listName;
      for (let j = 0; j < this.listGet[i].options.length; j++) { //boucle affichage des options
        this.json['pages'][0]['elements'][i + 1].choices[j] = this.listGet[i].options[j].optionName;
      }
    }
    this.surveyModel = new Survey.Model(this.json);
    Survey.SurveyNG.render("surveyElement", { model: this.surveyModel });//crée le modèle survey pour angular ?
    console.log('render', Survey.SurveyNG.render("surveyElement", { model: this.surveyModel }))

    this.surveyModel.onComplete
      .add(
        result => {
          this.resForm = JSON.parse(JSON.stringify(result.data));
          console.log('JSON.stringify(result.data) :', JSON.parse(JSON.stringify(result.data)))
          console.log('petit nom du formulaire à récupérer :', JSON.parse(JSON.stringify(result.data))['nomForm'])
          document.querySelector('#surveyResult').innerHTML = "result: " + result.data;
          document.querySelector('#surveyResult').innerHTML = "result: " + JSON.parse(JSON.stringify(result.data));
          console.log('surveyModel après query selector', this.surveyModel['pagesValue'][0].elements[0])
          console.log('surveyModel après query selector essai nom', this.surveyModel['pagesValue'][0].elements[0].name.value)
        })
  }
 
  notify() {
    console.log('resForm', this.resForm)
    this.activeModal.close(this.resForm);

  }

}



//Affichage pas à pas de toutes les listes dans surveyForm avec boucles d'option
    // this.json['pages'][0]['elements'][1].name = this.listGet[0].listName;
    // for (let j=0; j< this.listGet[0].options.length; j++){ //boucle affichage des options
    //   this.json['pages'][0]['elements'][1].choices[j] = this.listGet[0].options[j].optionName;
    // } 

    // this.json['pages'][0]['elements'][2].name = this.listGet[1].listName;
    // for (let j=0; j< this.listGet[1].options.length; j++){ //boucle affichage des options
    //   this.json['pages'][0]['elements'][2].choices[j] = this.listGet[1].options[j].optionName;
    // } 

    // this.json['pages'][0]['elements'][3].name = this.listGet[2].listName;
    // for (let j=0; j< this.listGet[2].options.length; j++){ //boucle affichage des options
    //   this.json['pages'][0]['elements'][3].choices[j] = this.listGet[2].options[j].optionName;
    // } 

    // this.json['pages'][0]['elements'][4].name = this.listGet[3].listName;
    // for (let j=0; j< this.listGet[3].options.length; j++){ //boucle affichage des options
    //   this.json['pages'][0]['elements'][4].choices[j] = this.listGet[3].options[j].optionName;
    // } 