import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SurveyModalComponent } from '../survey-modal/survey-modal.component';
import { SurveysService } from '../surveys.service';
import * as Survey from 'survey-angular';

@Component({
  selector: 'survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  SurveyFormGet : {} ;
  SurveyRetour : Object;
  surveyListR : any[];

  @Input() resForm : Object;

  constructor(private modalService: NgbModal,
              private surveysService :SurveysService ) { }

  ngOnInit() {
    this.get();
    this.surveyListR =  this.surveysService.surveyList;
    console.log('surveyList du service après création', this.surveysService.surveyList)
  }

  get() {
    this.SurveyFormGet = this.surveysService.getSurvey();
  }

  open(elemSurvey?:Object){
    const modalRef = this.modalService.open(SurveyModalComponent);
    // console.log ('avant la promise SurveyFormGet =', this.SurveyFormGet)
    // console.log('SurveyFormGet elements', this.SurveyFormGet['pages'][0]['elements'] )
   
   
   
    if(elemSurvey){
      //modalRef.componentInstance.json = this.SurveyFormGet;
      console.log ('dans le if open(elemsurvey)');
      //const surveyModel2 = new Survey.Model(this.SurveyFormGet);
     // console.log('surveyModel 2', surveyModel2)
      let copySurvey = JSON.parse(JSON.stringify(elemSurvey));
      //this.SurveyFormGet['pages'][0]['elements'] =JSON.parse(JSON.stringify(elemSurvey));
      //console.log('coucou elemSurvey', elemSurvey )
      console.log('copySurvey', copySurvey) // = surveyListR[0]
     // console.log ('copySurvey 2 =', this.SurveyFormGet['pages'][0]['elements'])
     
      //modalRef.componentInstance.surveyListR = elemSurvey;
      modalRef.componentInstance.json = this.SurveyFormGet;
      modalRef.componentInstance.contenuCopy = copySurvey;

      //console.log ('modalref',this.surveyListR)
      console.log('modal ref',this.SurveyFormGet['pages'][0]['elements'] )
    }
    else {
      modalRef.componentInstance.json = this.SurveyFormGet;
      console.log('dans le else -open(elemsurvey)')
    }


    modalRef.result.then((resSurvey) => { 
      console.log('essai de récup de resSurvey, retour du child',resSurvey);
      this.SurveyRetour = resSurvey;
      this.surveysService.addSurvey(resSurvey); 
      console.log('essai addSurvey', resSurvey);
      },
    (reason) => {
    });
  }

  // onSurveySaved(surveySaved){
  //   console.log('surveySaved = ', surveySaved)
  //   this.SurveyFormGet = surveySaved;
  // }

  
}
