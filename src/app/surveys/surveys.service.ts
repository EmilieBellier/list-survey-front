import { Injectable } from '@angular/core';

@Injectable()
export class SurveysService {

  titleForm= "Saisie du formulaire";
  listName = "";
  surveyList : any[] = []; //liste / tableau de tous les questionnaires survey

  public SurveyForm = {
    title: this.titleForm, //titre général du formulaire
    showProgressBar: "top",
    pages: [

      {//page 1 
        name: "Page 1 formulaire", // pour récupérer et lier ?
        title: "Page 1 formulaire", //celui qui s'affiche
        elements: [
          {
            type: "text",
            name: "nomForm",
            // title:"LabelNomForm",
            // placeholder : "youpi",
            // valueName :"par défaut"
          
          },
          {
            type: "dropdown",
            name: "", //Liste 1
            hasOther: true,
            choices: []
          },
          {
            type: "checkbox",
            name: "", //Liste 2
            hasOther: true,
            choices: []
          },{
            type: "dropdown",
            name: "", //Liste 3
            hasOther: true,
            choices: []
          },{
            type: "checkbox",
            name: "", //Liste 4
            hasOther: true,
            choices: []
          }


        ]
      },
    ]

  };
  constructor() { }

  getSurvey() {
    return (this.SurveyForm);
  }


  addSurvey (res : Object) { //res correspond à resSurvey du survey.ts
    console.log('objet reçu au service (resSurvey', res)
    this.surveyList.push(res);
    console.log('après le push, surveyList ', this.surveyList)
    console.log('après le push, surveylist [0]', this.surveyList[0])
  }

}
