import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/finally';
import { ListesService } from '../listes.service';
import { List } from '../data-model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { element } from 'protractor';
import { ListViewComponent } from '../list-view/list-view.component';

@Component({
  selector: 'lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  
  lists: Observable<List[]>;

  constructor(private listService: ListesService,
    private modalService: NgbModal) { } //NgbModal permet d'obtenir la méthode .open pour ouvrir un modal

  ngOnInit() {
    this.get();//copie de la listes (celle du service)
  }

  // CRUD en front

  // R
  get() {
    this.lists = this.listService.getLists();
  }

 // C & U / Création et update de listes :
  open(elementlist?: List) {  
    const modalRef = this.modalService.open(ListViewComponent);// ouverture du modal (vide)
    if(elementlist) //si liste existante (elementlist en paramètre), ouvre avec le contenu de la liste
    {
      let copyList = JSON.parse(JSON.stringify(elementlist));//stringify transforme l'objet elementlist en string
      //parse remet la string en objet
      //permet de modifier elementlist avant de l'envoyer dans selectedList pour eviter que ngModel modifie en meme temps la liste du modal et celle de la vue
      modalRef.componentInstance.list = copyList; //envoie de la copie de la liste dans l'@Input de listView
    }
    
    modalRef.result.then((resList) => { // promise. création et modification de liste, à la clôture du modal (close dans lisView)
      if (resList) { //resList = liste en retour (param de .close dans listViewComp)
        if (typeof resList.id !== 'undefined') { //vérification de l'existence de la propriété "id" (et non pas sa valeur !!)
          this.listService.updateList(resList).subscribe(); //si existe --> update
        }
        else {
          this.listService.addList(resList); //si n'existe pas --> création de la liste
        }
      }
    }, (reason) => {
    });
  }

  // D / Effacer la liste
  delete(elementlist: List) {
    this.listService.deleteList(elementlist);
  }
}



